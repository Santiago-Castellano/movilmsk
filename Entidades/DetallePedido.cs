﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class DetallePedido
    {
        public int Id { get; set; }

        public int IdPedido { get; set; }

        public int IdProducto { get; set; }

        public decimal? Cantidad { get; set; }

        public decimal? Kilos { get; set; }

        public decimal? Precio { get; set; }
        
        public decimal Total { get; set; }

        public string DescripcionPrecio { get; set; }
        
        public decimal? Descuento { get; set; }

        public Producto Producto { get; set; }

        public DetallePedido()
        {
            Producto = new Producto();
        }

    }
}
