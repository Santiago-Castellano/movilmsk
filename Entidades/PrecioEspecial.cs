﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class PrecioEspecial
    {
        public int Id { get; set; }
        public int IdEmpresa { get; set; }
        public decimal Precio { get; set; }
        public string Descripcion { get; set; }
        public int CodigoCliente { get; set; }
        public string CodigoProducto { get; set; }
    }
}
