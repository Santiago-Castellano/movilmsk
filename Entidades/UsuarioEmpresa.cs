﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class UsuarioEmpresa
    {
        public string Email { get; set; }
        public string Rol { get; set; }
        public int IdEmpresa { get; set; }
        public int NroVendedor { get; set; }
    }
}
