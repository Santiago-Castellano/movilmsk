﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Sincronizacion
    {
        public int Id { get; set; }
        public int Modelo { get; set; }
        public string NombreModelo { get; set; }
        public string Importacion { get; set; }
        public string Exportacion { get; set; }
        public bool TieneImportacion { get; set; }
        public bool TieneExportacion { get; set; }
    }
}
