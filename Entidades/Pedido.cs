﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Pedido
    {
        public int Id { get; set; }

        public int IdEmpresa { get; set; }

        public int NroVendedor { get; set; }

        public int IdCliente { get; set; }

        public string RazonSocial { get; set; }

        public string Cuit { get; set; }

        public string ObservacionCliente { get; set; }

        public string SituacionIva { get; set; }

        public int NroPedido { get; set; }

        public string Fecha { get; set; }

        public string Observaciones { get; set; }

        public List<DetallePedido> Detalles { get; set; }

        public Pedido()
        {
            Detalles = new List<DetallePedido>();
        }

    }
}
