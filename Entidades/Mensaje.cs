﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Clases
{
    public enum Modelos
    {
        Empresa,
        Cliente,
        Pedido,
        Usuario,
        Producto,
        PrecioEspecial
    }
    public static class Estados
    {
        public const string
            Importando = "Importando",
            Exportando = "Exportando",
            Nada = "Nada";
    }

    public static class Mensaje
    {
        private static string _NoExiste = "No Existe ";

        public static string NoExiste(Modelos modelo)
        {
            switch (modelo)
            {
                case Modelos.PrecioEspecial:
                    return _NoExiste + "El Precio Especial";
                case Modelos.Empresa:
                    return _NoExiste + "La Empresa";
                case Modelos.Cliente:
                    return _NoExiste + "El Cliente";
                case Modelos.Pedido:
                    return _NoExiste + "El Pedido";
                case Modelos.Usuario:
                    return _NoExiste + "El Usuario";
                case Modelos.Producto:
                    return _NoExiste + "El Producto";
                default:
                    return _NoExiste;
            }
        }


        public static string NombreModelo(Modelos modelo)
        {
            switch (modelo)
            {
                case Modelos.Empresa:
                    return "Empresa";
                case Modelos.Cliente:
                    return "Cliente";
                case Modelos.Pedido:
                    return "Pedido";
                case Modelos.Usuario:
                    return "Usuario";
                case Modelos.Producto:
                    return "Producto";
                case Modelos.PrecioEspecial:
                    return "PrecioEspecial";
                default:
                    return "";
            }
        }



    }
}
