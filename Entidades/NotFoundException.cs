﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Clases
{
    public class NotFoundException : Exception
    {
            
        public NotFoundException(Modelos modelo) : base(Mensaje.NoExiste(modelo))
        {
        }

    }
}
