﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class Cliente
    {
        public int Id { get; set; }
        public int Codigo { get; set; }
        public string RazonSocial { get; set; }
        public string Direccion { get; set; }
        public string Zona { get; set; }
        public string Telefono { get; set; }
        public decimal DescuentoFijo { get; set; }
        public string TipoCliente { get; set; }
        public string Localidad { get; set; }
        public string CondicionVenta { get; set; }
        public int NroVendedor { get; set; }
        public bool Lunes { get; set; }
        public bool Martes { get; set; }
        public bool Miercoles { get; set; }
        public bool Jueves { get; set; }
        public bool Viernes { get; set; }
        public bool Sabado { get; set; }
        public bool Domingo { get; set; }
        public string Cuit { get; set; }
        public string Observacion { get; set; }


    }
}
