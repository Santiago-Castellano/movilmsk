﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class Producto
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string Linea { get; set; }
        public string Rubro { get; set; }
        public string UnidadMedida { get; set; }
        public decimal PesoPromedio { get; set; }
        public decimal StockKilos { get; set; }
        public decimal StockCantidad { get; set; }
        public decimal Precio { get; set; }

        public List<PrecioEspecial> Precios { get; set; }
        public Producto()
        {
            this.Precios = new List<PrecioEspecial>();
        }
    }
}
