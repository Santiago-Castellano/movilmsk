namespace AccesoDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Productos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Productos()
        {
            DetallesPedidos = new HashSet<DetallesPedidos>();
        }

        public int Id { get; set; }

        public int IdEmpresa { get; set; }

        [Required]
        public string Codigo { get; set; }

        public string Descripcion { get; set; }

        public string Linea { get; set; }

        public string Rubro { get; set; }

        public string UnidadMedida { get; set; }

        public decimal? PesoPromedio { get; set; }

        public decimal? StockKilos { get; set; }

        public decimal? Precio { get; set; }

        public decimal? StockCantidad { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetallesPedidos> DetallesPedidos { get; set; }

        public virtual Empresas Empresas { get; set; }
    }
}
