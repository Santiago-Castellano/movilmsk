namespace AccesoDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PreciosEspeciales
    {
        public int id { get; set; }

        public int idEmpresa { get; set; }

        public int CodigoCliente { get; set; }

        [Required]
        public string CodigoProducto { get; set; }

        public decimal Precio { get; set; }

        [Required]
        public string Descripcion { get; set; }

        public virtual Empresas Empresas { get; set; }
    }
}
