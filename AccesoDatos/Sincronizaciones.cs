namespace AccesoDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Sincronizaciones
    {
        public int Id { get; set; }

        public int Modelo { get; set; }

        public DateTime Importacion { get; set; }

        public DateTime Exportacion { get; set; }

        public bool TieneImportacion { get; set; }

        public bool TieneExportacion { get; set; }

        public int IdEmpresa { get; set; }

        public string NombreModelo { get; set; }

        public string Estado { get; set; }

        public virtual Empresas Empresas { get; set; }
    }
}
