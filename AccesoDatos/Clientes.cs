namespace AccesoDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Clientes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Clientes()
        {
            Pedidos = new HashSet<Pedidos>();
        }

        public int Id { get; set; }

        public int IdEmpresa { get; set; }

        public int Codigo { get; set; }

        [Required]
        public string Cuit { get; set; }

        public int? NroVendedor { get; set; }

        public string RazonSocial { get; set; }

        public string Direccion { get; set; }

        public string Zona { get; set; }

        public string Telefono { get; set; }

        public decimal? DescuentoFijo { get; set; }

        public string TipoCliente { get; set; }

        public string Localidad { get; set; }

        public string CondicionVenta { get; set; }

        public bool? Domingo { get; set; }

        public bool? Lunes { get; set; }

        public bool? Martes { get; set; }

        public bool? Miercoles { get; set; }

        public bool? Jueves { get; set; }

        public bool? Viernes { get; set; }

        public bool? Sabado { get; set; }

        public string Observacion { get; set; }

        public virtual Empresas Empresas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pedidos> Pedidos { get; set; }
    }
}
