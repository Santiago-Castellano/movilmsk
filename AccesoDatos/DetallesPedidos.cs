namespace AccesoDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DetallesPedidos
    {
        public int Id { get; set; }

        public int IdPedido { get; set; }

        public int IdProducto { get; set; }

        public decimal? Cantidad { get; set; }

        public decimal? Kilos { get; set; }

        public decimal? Precio { get; set; }

        public decimal? Descuento { get; set; }

        public string DescripcionPrecio { get; set; }

        public virtual Pedidos Pedidos { get; set; }

        public virtual Productos Productos { get; set; }
    }
}
