namespace AccesoDatos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agrega_estado : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sincronizaciones", "Estado", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sincronizaciones", "Estado");
        }
    }
}
