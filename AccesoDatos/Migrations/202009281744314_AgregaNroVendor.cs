namespace AccesoDatos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregaNroVendor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsersEmpresas", "NroVendedor", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsersEmpresas", "NroVendedor");
        }
    }
}
