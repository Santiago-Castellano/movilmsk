namespace AccesoDatos
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model : DbContext
    {
        public Model()
            : base("name=dbModel")
        {
        }

        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<AspNetUsersEmpresas> AspNetUsersEmpresas { get; set; }
        public virtual DbSet<Clientes> Clientes { get; set; }
        public virtual DbSet<DetallesPedidos> DetallesPedidos { get; set; }
        public virtual DbSet<Empresas> Empresas { get; set; }
        public virtual DbSet<Pedidos> Pedidos { get; set; }
        public virtual DbSet<PreciosEspeciales> PreciosEspeciales { get; set; }
        public virtual DbSet<Productos> Productos { get; set; }
        public virtual DbSet<Sincronizaciones> Sincronizaciones { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRoles>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUsersEmpresas)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.IdAspNetUsers);

            modelBuilder.Entity<Clientes>()
                .Property(e => e.Cuit)
                .IsUnicode(false);

            modelBuilder.Entity<Clientes>()
                .Property(e => e.RazonSocial)
                .IsUnicode(false);

            modelBuilder.Entity<Clientes>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Clientes>()
                .Property(e => e.Zona)
                .IsUnicode(false);

            modelBuilder.Entity<Clientes>()
                .Property(e => e.Telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Clientes>()
                .Property(e => e.DescuentoFijo)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Clientes>()
                .Property(e => e.TipoCliente)
                .IsUnicode(false);

            modelBuilder.Entity<Clientes>()
                .Property(e => e.Localidad)
                .IsUnicode(false);

            modelBuilder.Entity<Clientes>()
                .Property(e => e.CondicionVenta)
                .IsUnicode(false);

            modelBuilder.Entity<Clientes>()
                .HasMany(e => e.Pedidos)
                .WithRequired(e => e.Clientes)
                .HasForeignKey(e => e.IdCliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DetallesPedidos>()
                .Property(e => e.Cantidad)
                .HasPrecision(12, 3);

            modelBuilder.Entity<DetallesPedidos>()
                .Property(e => e.Kilos)
                .HasPrecision(12, 3);

            modelBuilder.Entity<DetallesPedidos>()
                .Property(e => e.Precio)
                .HasPrecision(12, 3);

            modelBuilder.Entity<DetallesPedidos>()
                .Property(e => e.Descuento)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Empresas>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Empresas>()
                .HasMany(e => e.AspNetUsersEmpresas)
                .WithRequired(e => e.Empresas)
                .HasForeignKey(e => e.IdEmpresa);

            modelBuilder.Entity<Empresas>()
                .HasMany(e => e.Clientes)
                .WithRequired(e => e.Empresas)
                .HasForeignKey(e => e.IdEmpresa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Empresas>()
                .HasMany(e => e.Pedidos)
                .WithRequired(e => e.Empresas)
                .HasForeignKey(e => e.IdEmpresa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Empresas>()
                .HasMany(e => e.PreciosEspeciales)
                .WithRequired(e => e.Empresas)
                .HasForeignKey(e => e.idEmpresa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Empresas>()
                .HasMany(e => e.Productos)
                .WithRequired(e => e.Empresas)
                .HasForeignKey(e => e.IdEmpresa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Empresas>()
                .HasMany(e => e.Sincronizaciones)
                .WithRequired(e => e.Empresas)
                .HasForeignKey(e => e.IdEmpresa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pedidos>()
                .Property(e => e.Observaciones)
                .IsUnicode(false);

            modelBuilder.Entity<Pedidos>()
                .HasMany(e => e.DetallesPedidos)
                .WithRequired(e => e.Pedidos)
                .HasForeignKey(e => e.IdPedido)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PreciosEspeciales>()
                .Property(e => e.CodigoProducto)
                .IsUnicode(false);

            modelBuilder.Entity<PreciosEspeciales>()
                .Property(e => e.Precio)
                .HasPrecision(12, 2);

            modelBuilder.Entity<PreciosEspeciales>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Productos>()
                .Property(e => e.Codigo)
                .IsUnicode(false);

            modelBuilder.Entity<Productos>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Productos>()
                .Property(e => e.Linea)
                .IsUnicode(false);

            modelBuilder.Entity<Productos>()
                .Property(e => e.Rubro)
                .IsUnicode(false);

            modelBuilder.Entity<Productos>()
                .Property(e => e.UnidadMedida)
                .IsUnicode(false);

            modelBuilder.Entity<Productos>()
                .Property(e => e.PesoPromedio)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Productos>()
                .Property(e => e.StockKilos)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Productos>()
                .Property(e => e.Precio)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Productos>()
                .Property(e => e.StockCantidad)
                .HasPrecision(12, 3);

            modelBuilder.Entity<Productos>()
                .HasMany(e => e.DetallesPedidos)
                .WithRequired(e => e.Productos)
                .HasForeignKey(e => e.IdProducto)
                .WillCascadeOnDelete(false);
        }
    }
}
