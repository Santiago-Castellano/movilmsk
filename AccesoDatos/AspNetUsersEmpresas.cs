namespace AccesoDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AspNetUsersEmpresas
    {
        [Key]
        [Column(Order = 0)]
        public string IdAspNetUsers { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdEmpresa { get; set; }

        public int NroVendedor { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }

        public virtual Empresas Empresas { get; set; }
    }
}
