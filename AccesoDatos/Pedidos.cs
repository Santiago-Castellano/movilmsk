namespace AccesoDatos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Pedidos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Pedidos()
        {
            DetallesPedidos = new HashSet<DetallesPedidos>();
        }

        public int Id { get; set; }

        public int IdEmpresa { get; set; }

        public int NroVendedor { get; set; }

        public int IdCliente { get; set; }

        public int NroPedido { get; set; }

        public DateTime? Fecha { get; set; }

        public string Observaciones { get; set; }

        public virtual Clientes Clientes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetallesPedidos> DetallesPedidos { get; set; }

        public virtual Empresas Empresas { get; set; }
    }
}
