﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces
{
    public interface IPedidoServicio
    {
        void Borrar(int Id);
        Pedido ObtenerPorId(int id);
        Pedido Agregar(Pedido entidad);
        Pedido Editar(Pedido entidad);
        List<Pedido> Obtener(string idUser, int idEmpresa);
        string Exportar();
    }
}
