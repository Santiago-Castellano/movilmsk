﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces
{
    public interface IAuthServicio
    {
        bool ExisteVinculo(string idUsuario, int idEmpresa);
        int ObtenerEmpresaId();
        int ObtenerNroVendedor(int idEmpresa, string idUser);
    }
}
