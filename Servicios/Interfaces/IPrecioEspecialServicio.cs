﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces
{
    public interface IPrecioEspecialServicio : IServicio<PrecioEspecial>
    {
        void Importar(string ruta);
        List<PrecioEspecial> Obtener(int codigoCliente, string codigoProducto, int idEmpresa);
    }
}
