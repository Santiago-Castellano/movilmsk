﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Servicios.Interfaces
{
    public interface IClienteServicio 
    {
        void Borrar(int Id);
        List<Cliente> Obtener(string idUser);
        Cliente ObtenerPorId(int id);
        Cliente Agregar(Cliente entidad);
        Cliente Editar(Cliente entidad);
        void Importar(string ruta);
    }
}
