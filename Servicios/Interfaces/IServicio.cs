﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces
{
    public interface IServicio<TEntity> where TEntity : class
    {
        void Borrar(int Id);
        List<TEntity> Obtener();
        TEntity ObtenerPorId(int id);
        TEntity Agregar(TEntity entidad);
        TEntity Editar(TEntity entidad);
    }
}
