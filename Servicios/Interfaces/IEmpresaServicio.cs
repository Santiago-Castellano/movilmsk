﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces
{
    public interface IEmpresaServicio
    {
        void Borrar(int Id);
        List<Empresa> Obtener(string idUsuario);
        Empresa ObtenerPorId(int id);
        Empresa Agregar(Empresa empresa);
        Empresa Editar(Empresa empresa);
        void VincularUsuario(string email, string rol, int idEmpresa, int nroVendedor);
        void DesvincularUsuario(string email, int idEmpresa);
        List<UsuarioEmpresa> ObtenerUsuariosEmpresa(int idEmpresa);
    }
}
