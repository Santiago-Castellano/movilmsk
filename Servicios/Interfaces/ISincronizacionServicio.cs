﻿using Entidades;
using Servicios.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces
{
    public interface ISincronizacionServicio
    {
        List<Sincronizacion> Obtener();
        void ActualizarSincronizacion(int idEmpresa,Modelos modelo, bool importacion);
        void ActualizaEstadoSincronizacion(int idEmpresa, Modelos modelo, string estado);

    }
}
