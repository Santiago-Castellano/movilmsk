﻿using AccesoDatos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interfaces
{
    public interface IProductoServicio : IServicio<Producto>
    {
        void Importar(string ruta);
        List<string> ObtenerLineas();
        List<Producto> Obtener(int? codigoClietne = null);
    }
}
