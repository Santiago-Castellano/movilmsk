﻿using AccesoDatos;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace Servicios.Clases
{
    public class AuthServicio : IAuthServicio
    {
        internal DbContext contexto;
        internal DbSet<Empresas> dbSetEmpresa;
        internal DbSet<AspNetUsers> dbSetUsuario;
        internal DbSet<AspNetUsersEmpresas> dbSetEmpresaUsuario;

        public AuthServicio()
        {
            contexto = new Model();
            this.dbSetEmpresa = contexto.Set<Empresas>();
            this.dbSetUsuario = contexto.Set<AspNetUsers>();
            this.dbSetEmpresaUsuario = contexto.Set<AspNetUsersEmpresas>();
        }


        public bool ExisteVinculo(string idUsuario, int idEmpresa)
        {
            var vinculo = dbSetEmpresaUsuario.SingleOrDefault(v => v.IdAspNetUsers == idUsuario && v.IdEmpresa == idEmpresa);

            return vinculo != null;
        }

        public int ObtenerEmpresaId()
        {
            var headerIdEmpresa = HttpContext.Current.Request.Headers.GetValues("idEmpresa").First();
            if (int.TryParse(headerIdEmpresa, out int idEmpresa))
            {
                var vinculo = dbSetEmpresaUsuario.SingleOrDefault(v => v.IdEmpresa == idEmpresa && v.AspNetUsers.UserName == HttpContext.Current.User.Identity.Name);
                if (vinculo != null)
                    return idEmpresa;            
            }

            throw new NotFoundException(Modelos.Empresa);
        }
        public int ObtenerNroVendedor(int idEmpresa, string idUser)
        {
            var vinculo = dbSetEmpresaUsuario.SingleOrDefault(v => v.IdEmpresa == idEmpresa && v.IdAspNetUsers == idUser);
            if (vinculo != null)
                return vinculo.NroVendedor;

            throw new NotFoundException(Modelos.Empresa);
        }
    }
}
