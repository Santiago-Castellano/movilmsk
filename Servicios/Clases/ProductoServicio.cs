﻿using AccesoDatos;
using Entidades;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Clases
{
    public class ProductoServicio : IProductoServicio
    {
        internal DbContext contexto;
        internal DbSet<Productos> dbSet;
        private IAuthServicio AuthServicio;
        private ISincronizacionServicio SincronizacionServicio;
        private IPrecioEspecialServicio PrecioEspecialServicio;
        public ProductoServicio(IAuthServicio _authServicio, ISincronizacionServicio _sincronizacionServicio, IPrecioEspecialServicio _precioEspecialServicio)
        {
            this.SincronizacionServicio = _sincronizacionServicio;
            this.AuthServicio = _authServicio;
            this.PrecioEspecialServicio = _precioEspecialServicio;

            contexto = new Model();
            this.dbSet = contexto.Set<Productos>();
        }

        public Producto Agregar(Producto entidad)
        {
            Productos producto = this.Convertir(entidad);
            dbSet.Add(producto);
            contexto.SaveChanges();

            return this.Convertir(producto);
        }

        public void Borrar(int Id)
        {
            Productos producto = this._ObtenerPorId(Id);

            if (contexto.Entry(producto).State == EntityState.Detached)
                dbSet.Attach(producto);

            dbSet.Remove(producto);
            contexto.SaveChanges();
        }

        public Producto Editar(Producto entidad)
        {
            var productos = this._ObtenerPorId(entidad.Id);

            this.Convertir(productos, entidad);

            dbSet.Attach(productos);
            contexto.Entry(productos).State = EntityState.Modified;
            contexto.SaveChanges();

            return this.Convertir(productos);
        }

        public void Importar(string ruta)
        {
            var idEmpresa = AuthServicio.ObtenerEmpresaId();
            try
            {
                var existentes = dbSet.Where(p => p.IdEmpresa == idEmpresa).ToList();
                string[] lineas = File.ReadAllLines(ruta);
                this.SincronizacionServicio.ActualizaEstadoSincronizacion(idEmpresa, Modelos.Producto, Estados.Importando);
                for (int i = 0; i < lineas.Length; i++)
                {
                    var valores = lineas[i].Split(',');
                    var producto = existentes.SingleOrDefault(p => p.Codigo == valores[0]);
                    if (producto != null)
                    {
                        ArmarProducto(valores, idEmpresa, producto);
                        dbSet.Attach(producto);
                        contexto.Entry(producto).State = EntityState.Modified;
                    }
                    else
                    {
                        producto = new Productos(); 
                        ArmarProducto(valores, idEmpresa, producto);
                        dbSet.Add(producto);
                    }

                }
                contexto.SaveChanges();
                File.Delete(ruta);
                this.SincronizacionServicio.ActualizarSincronizacion(idEmpresa, Modelos.Producto, true);
            }
            catch (NotFoundException ex)
            {
                this.SincronizacionServicio.ActualizaEstadoSincronizacion(idEmpresa, Modelos.Producto, Estados.Nada);
                File.Delete(ruta);
                
                throw ex;
            }
            catch (Exception ex)
            {

                this.SincronizacionServicio.ActualizaEstadoSincronizacion(idEmpresa, Modelos.Producto, Estados.Nada);
                File.Delete(ruta);
                
                throw ex;
            }
            
        }

        private void ArmarProducto(string[] valores, int idEmpresa, Productos productos)
        {
            productos.Codigo = valores[0];
            productos.Descripcion = valores[1];
            productos.Linea = valores[3];
            productos.Rubro = valores[4];
            productos.UnidadMedida = valores[5];

            if (decimal.TryParse(valores[6], out decimal peso))
                productos.PesoPromedio = peso;

            if (decimal.TryParse(valores[2], out decimal precio))
                productos.Precio = precio;

            if (decimal.TryParse(valores[7], out decimal stockCantidad))
                productos.StockCantidad = stockCantidad;
            if (decimal.TryParse(valores[8], out decimal stockKilos))
                productos.StockKilos = stockKilos;

            productos.IdEmpresa = idEmpresa;

        }

        public List<Producto> Obtener(int? codigoClietne = null)
        {
            var idEmpresa = AuthServicio.ObtenerEmpresaId();
            var datos = dbSet.Where(p => p.IdEmpresa == idEmpresa).ToList();
            if (codigoClietne.HasValue)
                return datos.Select(p => this.Convertir(p, codigoClietne.Value)).ToList();

            return datos.Select(p => this.Convertir(p)).ToList();
        }

        private Producto Convertir(Productos p, int codigoClietne)
        {
            var producto = this.Convertir(p);

            producto.Precios.AddRange(PrecioEspecialServicio.Obtener(codigoClietne, p.Codigo, p.IdEmpresa));

            return producto;
        }

        public Producto ObtenerPorId(int id)
        {
            return this.Convertir(this._ObtenerPorId(id));
        }

        private Productos _ObtenerPorId(int id)
        {
            Productos producto = dbSet.SingleOrDefault(p => p.Id == id);
            if (producto == null)
                throw new NotFoundException(Modelos.Producto);

            return producto;
        }

        private void Convertir(Productos productos, Producto producto)
        {
            productos.Linea = producto.Linea;
            productos.Rubro = producto.Rubro;
            productos.StockKilos = producto.StockKilos;
            productos.StockCantidad = producto.StockCantidad;
            productos.UnidadMedida = producto.UnidadMedida;
            productos.PesoPromedio = producto.PesoPromedio;
            productos.Precio = producto.Precio;
            productos.Codigo = producto.Codigo;
            productos.Descripcion = productos.Descripcion;
        }

        private Productos Convertir(Producto producto)
        {
            return new Productos
            {
                Id = producto.Id,
                Codigo = producto.Codigo,
                Descripcion = producto.Descripcion,
                Linea = producto.Linea,
                StockCantidad = producto.StockCantidad,
                StockKilos = producto.StockKilos,
                Precio = producto.Precio,
                UnidadMedida = producto.UnidadMedida,
                PesoPromedio = producto.PesoPromedio,
                Rubro = producto.Rubro,
                IdEmpresa = AuthServicio.ObtenerEmpresaId()
            };
        }

        private Producto Convertir(Productos productos)
        {
            var producto = new Producto
            {
                Id = productos.Id,
                Codigo = productos.Codigo,
                Descripcion = productos.Descripcion,
                Linea = productos.Linea,
                PesoPromedio = productos.PesoPromedio.HasValue ? productos.PesoPromedio.Value : 0,
                Precio = productos.Precio.HasValue ? productos.Precio.Value : 0,
                Rubro = productos.Rubro,
                StockKilos = productos.StockKilos.HasValue ? productos.StockKilos.Value : 0,
                StockCantidad = productos.StockCantidad.HasValue ? productos.StockCantidad.Value : 0,
                UnidadMedida = productos.UnidadMedida
            };
            producto.Precios.Add(new PrecioEspecial
            {
                Id = 0,
                CodigoCliente = 0,
                CodigoProducto = producto.Codigo,
                Descripcion = "Precio del Producto",
                IdEmpresa = productos.IdEmpresa,
                Precio = productos.Precio.HasValue ? productos.Precio.Value : 0
            });

            return producto;
        }

        public List<string> ObtenerLineas()
        {
            var idEmpresa = AuthServicio.ObtenerEmpresaId();
            var lineas = dbSet.Where(p => p.IdEmpresa == idEmpresa).Select(p => p.Linea).Distinct().OrderBy(l => l).ToList();
            return lineas;
        }

        public List<Producto> Obtener()
        {
            throw new NotImplementedException();
        }
    }
}
