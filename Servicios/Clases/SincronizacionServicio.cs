﻿using AccesoDatos;
using Entidades;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.DependencyResolution;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Clases
{
    public class SincronizacionServicio : ISincronizacionServicio
    {
        internal DbContext contexto;
        internal DbSet<Sincronizaciones> dbSet;
        private IAuthServicio AuthServicio;
        public SincronizacionServicio(IAuthServicio _authServicio)
        {
            this.AuthServicio = _authServicio;
            contexto = new Model();
            this.dbSet = contexto.Set<Sincronizaciones>();
        }
        private void _Agregar(Sincronizaciones sincronizacion)
        {
            if ((Modelos)sincronizacion.Modelo == Modelos.Pedido)
            {
                sincronizacion.TieneExportacion = true;
                sincronizacion.TieneImportacion = false;
            }
            else
            {
                sincronizacion.TieneImportacion = true;
                sincronizacion.TieneExportacion = false;
            }
            sincronizacion.Estado = "Nada";
            dbSet.Add(sincronizacion);
            contexto.SaveChanges();
        }
        public void ActualizaEstadoSincronizacion(int idEmpresa, Modelos modelo, string estado)
        {
            var sincronizacion = dbSet.SingleOrDefault(s => s.IdEmpresa == idEmpresa && s.Modelo == (int)modelo);
            sincronizacion.Estado = estado;
            dbSet.Attach(sincronizacion);
            contexto.Entry(sincronizacion).State = EntityState.Modified;
            contexto.SaveChanges();
        }

        public void ActualizarSincronizacion(int idEmpresa, Modelos modelo, bool importacion)
        {
            var sincronizacion = dbSet.SingleOrDefault(s => s.IdEmpresa == idEmpresa && s.Modelo == (int)modelo);
            if (sincronizacion == null)
            {
                sincronizacion = new Sincronizaciones
                {
                    IdEmpresa = idEmpresa,
                    Modelo = (int)modelo,
                    NombreModelo = Mensaje.NombreModelo(modelo),
                    Exportacion = DateTime.Now,
                    Importacion = DateTime.Now
                };
                this._Agregar(sincronizacion);
            };

            if (importacion)
                sincronizacion.Importacion = DateTime.Now;
            else
                sincronizacion.Exportacion = DateTime.Now;

            sincronizacion.Estado = "Nada";

            contexto.SaveChanges();
        }

        public List<Sincronizacion> Obtener()
        {
            var idEmpresa = AuthServicio.ObtenerEmpresaId();
            var lista = new List<Sincronizaciones>();
            foreach (var item in Enum.GetValues(typeof(Modelos)))
            {
                var nroModelo = 0;
                switch (item.ToString())
                {
                    case "Cliente":
                        nroModelo = 1;
                        break;
                    case "Pedido":
                        nroModelo = 2;
                        break;
                    case "Producto":
                        nroModelo = 4;
                        break;
                    case "PrecioEspecial":
                        nroModelo = 5;
                        break;
                    default:
                        break;
                }
                var sin = dbSet.SingleOrDefault(s => s.IdEmpresa == idEmpresa && s.Modelo == nroModelo);
                if (sin == null && nroModelo != 0)
                {
                    sin = new Sincronizaciones
                    {
                        IdEmpresa = idEmpresa,
                        Modelo = nroModelo,
                        NombreModelo = item.ToString(),
                        Exportacion = DateTime.Now,
                        Importacion = DateTime.Now
                    };
                    this._Agregar(sin);
                }
                if (nroModelo != 0)
                    lista.Add(sin);
            }

            return lista.Select(s => this.Convertir(s)).ToList();
        }

        private Sincronizacion Convertir(Sincronizaciones sincronizaciones)
        {
            return new Sincronizacion
            {
                Id = sincronizaciones.Id,
                Exportacion = sincronizaciones.Estado == "Exportando" ? sincronizaciones.Estado : sincronizaciones.Exportacion.ToString("dd-MM-yyyy HH:mm tt"),
                Importacion = sincronizaciones.Estado =="Importando" ? sincronizaciones.Estado  : sincronizaciones.Importacion.ToString("dd-MM-yyyy HH:mm tt"),
                Modelo = sincronizaciones.Modelo,
                NombreModelo = sincronizaciones.NombreModelo,
                TieneExportacion = sincronizaciones.TieneExportacion,
                TieneImportacion = sincronizaciones.TieneImportacion
            };
        }
    }
}
