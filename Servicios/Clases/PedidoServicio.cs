﻿using AccesoDatos;
using Entidades;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Servicios.Clases
{
    public class PedidoServicio : IPedidoServicio
    {
        private IAuthServicio AuthServicio;
        private ISincronizacionServicio SincronizacionServicio;
        private IPrecioEspecialServicio PrecioEspecialServicio;

        private DbContext contexto;
        private DbSet<Pedidos> dbSetPedidos;
        private DbSet<DetallesPedidos> dbSetDetalles;
        private DbSet<Clientes> dbSetCliente;
        private DbSet<AspNetUsersEmpresas> dbSetUsuarioEmpresa;
        private DbSet<Productos> dbSetProductos;


        public PedidoServicio(IAuthServicio _authServicio, ISincronizacionServicio _sincronizacionServicio, IPrecioEspecialServicio _PrecioEspecialServicio)
        {
            this.AuthServicio = _authServicio;
            this.SincronizacionServicio = _sincronizacionServicio;
            this.PrecioEspecialServicio = _PrecioEspecialServicio;

            this.contexto = new Model();
            this.dbSetPedidos = contexto.Set<Pedidos>();
            this.dbSetDetalles = contexto.Set<DetallesPedidos>();
            this.dbSetCliente = contexto.Set<Clientes>();
            this.dbSetUsuarioEmpresa = contexto.Set<AspNetUsersEmpresas>();
            this.dbSetProductos = contexto.Set<Productos>();
        }


        public Pedido Agregar(Pedido entidad)
        {
            var username = HttpContext.Current.User.Identity.Name;
            var vinculo = dbSetUsuarioEmpresa.SingleOrDefault(v => v.IdEmpresa == entidad.IdEmpresa && v.AspNetUsers.UserName == username);
            entidad.NroVendedor = vinculo.NroVendedor;
            Pedidos pedidos = this.Convertir(entidad);
            dbSetPedidos.Add(pedidos);
            contexto.SaveChanges();

            return this.Convertir(pedidos);
        }

        public void Borrar(int Id)
        {
            var pedido = dbSetPedidos.SingleOrDefault(p => p.Id == Id);
            var vinculo = dbSetUsuarioEmpresa.SingleOrDefault(r => r.IdEmpresa == pedido.IdEmpresa && r.AspNetUsers.UserName == HttpContext.Current.User.Identity.Name);
            if (vinculo == null)
                throw new NotFoundException(Modelos.Pedido);
            
            dbSetDetalles.RemoveRange(dbSetDetalles.Where(d => d.IdPedido == Id));
            dbSetPedidos.Remove(pedido);
            contexto.SaveChanges();
        }

        public Pedido Editar(Pedido entidad)
        {
            this.Borrar(entidad.Id);
            var editado = this.Agregar(entidad);
            return editado;
        }

        private Pedido Convertir(Pedidos pedidos)
        {
            if (pedidos.Clientes == null)
                pedidos.Clientes = dbSetCliente.SingleOrDefault(c => c.Id == pedidos.IdCliente && c.IdEmpresa == pedidos.IdEmpresa);

            var pedido = new Pedido
            {
                Id = pedidos.Id,
                IdCliente = pedidos.IdCliente,
                IdEmpresa = pedidos.IdEmpresa,
                Cuit = pedidos.Clientes.Cuit,
                RazonSocial = pedidos.Clientes.RazonSocial,
                Fecha = pedidos.Fecha.Value.ToString("dd/MM/yyyy"),
                Observaciones = pedidos.Observaciones,
                ObservacionCliente = pedidos.Clientes.Observacion
            };
            int codigoCliente = pedidos.Clientes.Codigo;
            foreach (var detalles in pedidos.DetallesPedidos)
            {
                pedido.Detalles.Add(this.Convertir(detalles,codigoCliente));
            }

            return pedido;
        }

        private DetallePedido Convertir(DetallesPedidos detalles,int codigoCliente)
        {
            if (detalles.Productos == null)
                detalles.Productos = dbSetProductos.SingleOrDefault(p => p.Id == detalles.IdProducto && p.IdEmpresa == detalles.Pedidos.IdEmpresa);

            var detalle =  new DetallePedido
            {
                Id = detalles.Id,
                Cantidad = detalles.Cantidad,
                Kilos = detalles.Kilos,
                Precio = detalles.Precio,
                DescripcionPrecio = detalles.DescripcionPrecio,
                IdPedido = detalles.IdPedido,
                IdProducto = detalles.IdProducto,
                Total = ((detalles.Productos.UnidadMedida == "K" ? detalles.Kilos.Value : detalles.Cantidad.Value) * detalles.Precio).Value
            };
            var productos = detalles.Productos;

            var producto = new Producto
            {
                Id = productos.Id,
                Codigo = productos.Codigo,
                Descripcion = productos.Descripcion,
                Linea = productos.Linea,
                PesoPromedio = productos.PesoPromedio.HasValue ? productos.PesoPromedio.Value : 0,
                Precio = productos.Precio.HasValue ? productos.Precio.Value : 0,
                Rubro = productos.Rubro,
                StockKilos = productos.StockKilos.HasValue ? productos.StockKilos.Value : 0,
                StockCantidad = productos.StockCantidad.HasValue ? productos.StockCantidad.Value : 0,
                UnidadMedida = productos.UnidadMedida
            };
            detalle.Producto = producto;

            producto.Precios.Add(new PrecioEspecial
            {
                Id = 0,
                CodigoCliente = 0,
                CodigoProducto = producto.Codigo,
                Descripcion = "Precio del Producto",
                IdEmpresa = productos.IdEmpresa,
                Precio = productos.Precio.HasValue ? productos.Precio.Value : 0
            });

            producto.Precios.AddRange(PrecioEspecialServicio.Obtener(codigoCliente,producto.Codigo,productos.IdEmpresa));

            return detalle;
        }

        private Pedidos Convertir(Pedido entidad)
        {
            if (entidad.IdCliente == 0)
            {
                var nuevoCliente = new Clientes
                {
                    RazonSocial = entidad.RazonSocial,
                    Cuit = entidad.Cuit,
                    IdEmpresa = entidad.IdEmpresa,
                    Codigo = 0,
                    CondicionVenta = entidad.SituacionIva,
                    Observacion = entidad.ObservacionCliente != null ? entidad.ObservacionCliente.Replace("\n"," ") : null,
                    NroVendedor = entidad.NroVendedor
                };
                dbSetCliente.Add(nuevoCliente);
                contexto.SaveChanges();
                entidad.IdCliente = nuevoCliente.Id;
            }

            var pedidos = new Pedidos
            {
                IdCliente = entidad.IdCliente,
                IdEmpresa = entidad.IdEmpresa,
                Fecha = DateTime.Now,
                NroVendedor = entidad.NroVendedor,
                Observaciones = entidad.Observaciones
            };
            foreach (var detalle in entidad.Detalles)
            {
                pedidos.DetallesPedidos.Add(this.Convertir(detalle));
            }

            return pedidos;
        }

        private DetallesPedidos Convertir(DetallePedido detalle)
        {
            return new DetallesPedidos
            {
                Cantidad = detalle.Cantidad,
                Kilos = detalle.Kilos,
                DescripcionPrecio = detalle.DescripcionPrecio,
                Precio = detalle.Precio,
                IdProducto = detalle.IdProducto,
                IdPedido = detalle.IdPedido
            };
        }

      

        public List<Pedido> Obtener(string idUser, int idEmpresa)
        {
            var vinculo = dbSetUsuarioEmpresa.SingleOrDefault(u => u.IdAspNetUsers == idUser && u.IdEmpresa == idEmpresa);
            var pedidos = dbSetPedidos.Where(p => p.NroVendedor == vinculo.NroVendedor && p.IdEmpresa == idEmpresa).ToList();

            return pedidos.Select(p => this.Convertir(p)).ToList();
        }

        public Pedido ObtenerPorId(int id)
        {
            throw new NotImplementedException();
        }

        private string ArmarLineaExportacion(Pedidos pedido, DetallesPedidos item)
        {
            string linea = $"{pedido.Id}," +
                    $"{pedido.Clientes.Codigo},{pedido.Clientes.RazonSocial},{pedido.Clientes.Cuit}," +
                    $"{(pedido.Clientes.Observacion != null ? pedido.Clientes.Observacion : string.Empty)}," +
                    $"{pedido.NroVendedor},{pedido.Fecha.Value.ToString("dd/MM/yyyy HH:mm")}," +
                    $"{item.Productos.Codigo}, {item.Cantidad}, {item.Kilos}," +
                    $"{item.Precio}, {item.DescripcionPrecio}, {pedido.Observaciones}";
            
            return linea;
        }

        public string Exportar()
        {
            var idEmpresa = this.AuthServicio.ObtenerEmpresaId();
            try
            {
                this.SincronizacionServicio.ActualizaEstadoSincronizacion(idEmpresa, Modelos.Pedido, Estados.Exportando);
                var c = HttpContext.Current;
                var root = c.Server.MapPath("~/App_Data/" + idEmpresa.ToString());
                if (!Directory.Exists(root))
                    Directory.CreateDirectory(root);

                var nombre = $"Pedidos_{DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss")}.csv";
                TextWriter archivo = new StreamWriter(root + "/" + nombre);
                string datos = "Id_Pedido, Codigo_Cliente, Razon_Social, CUIT,ObservacionCliente,Nro_Vendedor, Fecha_Pedido, Codigo_Producto, Cantidad, Kilos, Precio, Descripcion_Precio, Observacion \n";
                foreach (var pedido in dbSetPedidos.Where(p => p.IdEmpresa == idEmpresa))
                {
                    foreach (var detalle in pedido.DetallesPedidos)
                    {
                        datos += ArmarLineaExportacion(pedido,detalle) + "\n";
                    }
                }
                archivo.Write(datos);
                archivo.Close();

                dbSetDetalles.RemoveRange(dbSetDetalles.Where(d => d.Pedidos.IdEmpresa == idEmpresa));
                dbSetPedidos.RemoveRange(dbSetPedidos.Where(d => d.IdEmpresa == idEmpresa));
                contexto.SaveChanges();

                this.SincronizacionServicio.ActualizarSincronizacion(idEmpresa, Modelos.Pedido, false);

                return root + "/" + nombre;
            }
            catch (Exception ex)
            {
                this.SincronizacionServicio.ActualizaEstadoSincronizacion(idEmpresa, Modelos.Pedido, Estados.Nada);

                throw ex;
            }
            
        }
    }
}
