﻿using AccesoDatos;
using Entidades;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Clases
{
    public class EmpresaServicio : IEmpresaServicio
    {
        internal DbContext contexto;
        internal DbSet<Empresas> dbSet;
        internal DbSet<AspNetUsers> dbSetUsuarios;
        internal DbSet<AspNetRoles> dbSetRoles;
        internal DbSet<AspNetUsersEmpresas> dbSetUsuarioEmpresa;

        public EmpresaServicio()
        {
            contexto = new Model();
            this.dbSet = contexto.Set<Empresas>();
            this.dbSetUsuarios = contexto.Set<AspNetUsers>();
            this.dbSetRoles = contexto.Set<AspNetRoles>();
            this.dbSetUsuarioEmpresa = contexto.Set<AspNetUsersEmpresas>();
        }

        public Empresa Agregar(Empresa empresa)
        {
            var empresaNueva = this._Convertir(empresa);
            dbSet.Add(empresaNueva);
            contexto.SaveChanges();

            return this._Convertir(empresaNueva);
        }

        public void Borrar(int Id)
        {
            var empresaBorrar = this.dbSet.SingleOrDefault(e => e.Id == Id);

            if (empresaBorrar == null)
                throw new NotFoundException(Modelos.Empresa);

            if (contexto.Entry(empresaBorrar).State == EntityState.Detached)
                dbSet.Attach(empresaBorrar);

            dbSet.Remove(empresaBorrar);
            contexto.SaveChanges();
        }

        public void DesvincularUsuario(string email, int idEmpresa)
        {
            var usuario = dbSetUsuarios.SingleOrDefault(u => u.Email == email);
            var empresa = dbSet.SingleOrDefault(e => e.Id == idEmpresa);
            if (empresa == null)
                throw new NotFoundException(Modelos.Empresa);
            if (usuario == null)
                throw new NotFoundException(Modelos.Usuario);

            var vinculo = this.dbSetUsuarioEmpresa.SingleOrDefault(v => v.IdEmpresa == idEmpresa && v.IdAspNetUsers == usuario.Id);
            this.dbSetUsuarioEmpresa.Remove(vinculo);
            contexto.SaveChanges();
        }

        public Empresa Editar(Empresa empresa)
        {
            var empresaEditar = this.dbSet.SingleOrDefault(e => e.Id == empresa.Id);
            if (empresaEditar == null)
                throw new NotFoundException(Modelos.Empresa);

            this._Convertir(empresaEditar, empresa);

            dbSet.Attach(empresaEditar);
            contexto.Entry(empresaEditar).State = EntityState.Modified;
            contexto.SaveChanges();

            return this._Convertir(empresaEditar);
        }

        public List<Empresa> Obtener(string idUsuario)
        {
            List<Empresas> lista = new List<Empresas>();
            if (idUsuario == string.Empty)
                lista = this.dbSet.ToList();
            else
            {
                var usuario = this.dbSetUsuarios.SingleOrDefault(u => u.Id == idUsuario);
                if (usuario == null)
                    throw new NotFoundException(Modelos.Usuario);
                lista = usuario.AspNetUsersEmpresas.Select(v => v.Empresas).ToList();
            }
            return lista.Select(e => this._Convertir(e)).ToList();
        }

        public Empresa ObtenerPorId(int id)
        {
            var empresa = this.dbSet.SingleOrDefault(e => e.Id == id);
            if (empresa == null)
                throw new NotFoundException(Modelos.Empresa);

            return this._Convertir(empresa);
        }

        public List<UsuarioEmpresa> ObtenerUsuariosEmpresa(int idEmpresa)
        {
            var empresa = dbSet.SingleOrDefault(e => e.Id == idEmpresa);
            if (empresa == null)
                throw new NotFoundException(Modelos.Empresa);

            return empresa.AspNetUsersEmpresas.Select(v => this._Convertir(v, idEmpresa)).ToList();
        }

        private UsuarioEmpresa _Convertir(AspNetUsersEmpresas v, int idEmpresa)
        {
            var rol = v.AspNetUsers.AspNetRoles.FirstOrDefault();
            return new UsuarioEmpresa
            {
                Email = v.AspNetUsers.Email,
                IdEmpresa = idEmpresa,
                Rol = rol != null ? rol.Name : "",
                NroVendedor= v.NroVendedor
            };
        }

        public void VincularUsuario(string email, string rol, int idEmpresa, int nroVendedor)
        {
            var usuario = dbSetUsuarios.SingleOrDefault(u => u.Email == email);
            if (usuario == null)
                throw new NotFoundException(Modelos.Usuario);
            if (usuario.AspNetRoles.Count == 0)
            {
                var rolEntity = dbSetRoles.SingleOrDefault(r => r.Name == rol);
                usuario.AspNetRoles.Add(rolEntity);
            }
            contexto.SaveChanges();

            var empresa = dbSet.SingleOrDefault(e => e.Id == idEmpresa);
            if (empresa == null)
                throw new NotFoundException(Modelos.Empresa);
            this.dbSetUsuarioEmpresa.Add(new AspNetUsersEmpresas
            {
                IdEmpresa = empresa.Id,
                IdAspNetUsers = usuario.Id,
                NroVendedor = nroVendedor
            });
            contexto.SaveChanges();
        }

        private Empresa _Convertir(Empresas empresa)
        {
            return new Empresa
            {
                Id = empresa.Id,
                Nombre = empresa.Nombre
            };
        }
        private Empresas _Convertir(Empresa empresa)
        {
            return new Empresas
            {
                Id = empresa.Id,
                Nombre = empresa.Nombre
            };
        }
        private void _Convertir(Empresas empresaEditar, Empresa empresa)
        {
            empresaEditar.Nombre = empresa.Nombre;
        }

    }
}
