﻿using AccesoDatos;
using Entidades;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace Servicios.Clases
{
    public class ClienteServicio : IClienteServicio
    {
        internal DbContext contexto;
        internal DbSet<Clientes> dbSet;
        private IAuthServicio AuthServicio;
        private ISincronizacionServicio SincronizacionServicio;
        public ClienteServicio(IAuthServicio _authServicio, ISincronizacionServicio _sincronizacionServicio)
        {
            this.SincronizacionServicio = _sincronizacionServicio;
            this.AuthServicio = _authServicio;
            contexto = new Model();
            this.dbSet = contexto.Set<Clientes>();
        }

        private void _agregar(Clientes clientes)
        {
            dbSet.Add(clientes);
            contexto.SaveChanges();
        }
        public Cliente Agregar(Cliente cliente)
        {
            var nuevoCliente = this.Convertir(cliente);

            this._agregar(nuevoCliente);

            return this.Convertir(nuevoCliente);
        }

        public void Borrar(int Id)
        {
            var clienteBorrar = this._ObtenerPorId(Id);

            if (contexto.Entry(clienteBorrar).State == EntityState.Detached)
                dbSet.Attach(clienteBorrar);

            dbSet.Remove(clienteBorrar);
            contexto.SaveChanges();
        }

        public Cliente Editar(Cliente cliente)
        {
            var clienteEditar = this._ObtenerPorId(cliente.Id);

            this.Convertir(clienteEditar, cliente);

            dbSet.Attach(clienteEditar);
            contexto.Entry(clienteEditar).State = EntityState.Modified;
            contexto.SaveChanges();

            return this.Convertir(clienteEditar);
        }

        public List<Cliente> Obtener(string idUser)
        {
            var idEmpresa = AuthServicio.ObtenerEmpresaId();
            var nroVendedor = AuthServicio.ObtenerNroVendedor(idEmpresa, idUser);
            var lista = this.dbSet.Where(c => c.IdEmpresa == idEmpresa && c.NroVendedor == nroVendedor).ToList();

            return lista.Select(cliente => this.Convertir(cliente)).ToList();
        }

        public Cliente ObtenerPorId(int id)
        {
            return this.Convertir(this._ObtenerPorId(id));
        }

        private Clientes _ObtenerPorId(int id)
        {
            var idEmpresa = AuthServicio.ObtenerEmpresaId();
            var cliente = this.dbSet.SingleOrDefault(c => c.IdEmpresa == idEmpresa && c.Id == id);
            if (cliente == null)
                throw new NotFoundException(Modelos.Cliente);

            return cliente;
        }

        public void Importar(string ruta)
        {
            var idEmpresa = AuthServicio.ObtenerEmpresaId();
            try
            {
                string[] lineas = File.ReadAllLines(ruta);
                var clientes = dbSet.Where(c => c.IdEmpresa == idEmpresa).ToList();
                this.SincronizacionServicio.ActualizaEstadoSincronizacion(idEmpresa, Modelos.Cliente, Estados.Importando);
                for (int i = 1; i < lineas.Length; i++)
                {
                    var valores = lineas[i].Split(',');
                    if (int.TryParse(valores[0], out int codCli))
                    {
                        var cliente = clientes.SingleOrDefault(c => c.Codigo == codCli);
                        if (cliente != null)
                        {
                            ArmarCliente(valores, idEmpresa, cliente);
                            dbSet.Attach(cliente);
                            contexto.Entry(cliente).State = EntityState.Modified;
                        }
                        else
                        {
                            cliente = new Clientes();
                            ArmarCliente(valores, idEmpresa, cliente);
                            dbSet.Add(cliente);
                        }
                    }
                }
                File.Delete(ruta);
                contexto.SaveChanges();
                this.SincronizacionServicio.ActualizarSincronizacion(idEmpresa, Modelos.Cliente, true);
            }
            catch (NotFoundException ex)
            {
                this.SincronizacionServicio.ActualizaEstadoSincronizacion(idEmpresa, Modelos.Cliente, Estados.Nada);
                File.Delete(ruta);
                throw ex;
            }
            catch (Exception ex)
            {
                this.SincronizacionServicio.ActualizaEstadoSincronizacion(idEmpresa, Modelos.Cliente, Estados.Nada);
                File.Delete(ruta);
                throw ex;
            }

            
        }

        private void ArmarCliente(string[] valores, int idEmpresa, Clientes cliente)
        {
            if (int.TryParse(valores[0], out int cod))
                cliente.Codigo = cod;
            cliente.RazonSocial = valores[1];
            cliente.Direccion = valores[2];
            cliente.Zona = valores[3];
            cliente.Telefono = valores[4];
           // if (decimal.TryParse(valores[5], out decimal descFijo))
           //     cliente.DescuentoFijo = descFijo;
            cliente.TipoCliente = valores[7];
            cliente.Localidad = valores[8];
            cliente.CondicionVenta = valores[12];
            if (int.TryParse(valores[14], out int nroVen))
                cliente.NroVendedor = nroVen;
            cliente.Cuit = valores[24];
            if (cliente.Cuit == string.Empty)
                cliente.Cuit = "Sin Asiganar";

            cliente.IdEmpresa = idEmpresa;
        }

        private Cliente Convertir(Clientes clientes)
        {
            var c = new Cliente
            {
                Codigo = clientes.Codigo,
                DescuentoFijo = clientes.DescuentoFijo.HasValue ? clientes.DescuentoFijo.Value : 0,
                Id = clientes.Id,
                RazonSocial = clientes.RazonSocial,
                Localidad = clientes.Localidad,
                Zona = clientes.Zona,
                CondicionVenta = clientes.CondicionVenta,
                Direccion = clientes.Direccion,
                Telefono = clientes.Telefono,
                TipoCliente = clientes.TipoCliente,
                NroVendedor = clientes.NroVendedor.HasValue ? clientes.NroVendedor.Value : 0,
                Lunes = clientes.Lunes.HasValue ? clientes.Lunes.Value : false,
                Martes = clientes.Martes.HasValue ? clientes.Martes.Value : false,
                Miercoles = clientes.Miercoles.HasValue ? clientes.Miercoles.Value : false,
                Jueves = clientes.Jueves.HasValue ? clientes.Jueves.Value : false,
                Viernes = clientes.Viernes.HasValue ? clientes.Viernes.Value : false,
                Sabado = clientes.Sabado.HasValue ? clientes.Sabado.Value : false,
                Domingo = clientes.Domingo.HasValue ? clientes.Domingo.Value : false,
                Cuit = clientes.Cuit,
                Observacion = clientes.Observacion
            };

            return c;
        }

        private Clientes Convertir(Cliente cliente)
        {

            return new Clientes
            {
                Codigo = cliente.Codigo,
                CondicionVenta = cliente.CondicionVenta,
                DescuentoFijo = cliente.DescuentoFijo,
                Direccion = cliente.Direccion,
                Id = cliente.Id,
                IdEmpresa = AuthServicio.ObtenerEmpresaId(),
                Localidad = cliente.Localidad,
                TipoCliente = cliente.TipoCliente,
                RazonSocial = cliente.RazonSocial,
                Telefono = cliente.Telefono,
                NroVendedor = cliente.NroVendedor,
                Lunes = cliente.Lunes,
                Martes = cliente.Martes,
                Miercoles = cliente.Miercoles,
                Jueves = cliente.Jueves,
                Viernes = cliente.Viernes,
                Sabado = cliente.Sabado,
                Domingo = cliente.Domingo,
                Cuit = cliente.Cuit,
                Observacion = cliente.Observacion
            };
        }

        private void Convertir(Clientes clienteEditar, Cliente cliente)
        {
            clienteEditar.Cuit = cliente.Cuit;
            clienteEditar.CondicionVenta = cliente.CondicionVenta;
            clienteEditar.DescuentoFijo = cliente.DescuentoFijo;
            clienteEditar.Direccion = cliente.Direccion;
            clienteEditar.Localidad = cliente.Localidad;
            clienteEditar.TipoCliente = cliente.TipoCliente;
            clienteEditar.RazonSocial = cliente.RazonSocial;
            clienteEditar.Telefono = cliente.Telefono;
            clienteEditar.NroVendedor = cliente.NroVendedor;
            clienteEditar.Lunes = cliente.Lunes;
            clienteEditar.Martes = cliente.Martes;
            clienteEditar.Miercoles = cliente.Miercoles;
            clienteEditar.Jueves = cliente.Jueves;
            clienteEditar.Viernes = cliente.Viernes;
            clienteEditar.Sabado = cliente.Sabado;
            clienteEditar.Domingo = cliente.Domingo;
            clienteEditar.Observacion = cliente.Observacion;
        }
    }
}
