﻿using AccesoDatos;
using Entidades;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Clases
{
    public class PrecioEspecialServicio : IPrecioEspecialServicio
    {
        private DbContext contexto;
        private DbSet<PreciosEspeciales> dbSet;
        private IAuthServicio AuthServicio;
        private ISincronizacionServicio SincronizacionServicio;
        public PrecioEspecialServicio(IAuthServicio _authServicio, ISincronizacionServicio _sincronizacionServicio)
        {
            this.SincronizacionServicio = _sincronizacionServicio;
            this.AuthServicio = _authServicio;
            this.contexto = new Model();
            this.dbSet = contexto.Set<PreciosEspeciales>();
        }
        public PrecioEspecial Agregar(PrecioEspecial precioAgregar)
        {
            var precio = this.Convertir(precioAgregar);

            dbSet.Add(precio);
            contexto.SaveChanges();

            return this.Convertir(precio);
        }

        public void Borrar(int Id)
        {
            var precio = _ObtenerPorId(Id);

            if (contexto.Entry(precio).State == EntityState.Detached)
                dbSet.Attach(precio);

            dbSet.Remove(precio);

            contexto.SaveChanges();
        }

        public PrecioEspecial Editar(PrecioEspecial precioEditado)
        {
            var precio = _ObtenerPorId(precioEditado.Id);
            this.Convertir(precio, precioEditado);

            dbSet.Attach(precio);
            contexto.Entry(precio).State = EntityState.Modified;
            contexto.SaveChanges();

            return this.Convertir(precio);
        }



        public void Importar(string ruta)
        {
            var idEmpresa = AuthServicio.ObtenerEmpresaId();
            try
            {
                dbSet.RemoveRange(dbSet.Where(p => p.idEmpresa == idEmpresa));
                contexto.SaveChanges();
                this.SincronizacionServicio.ActualizaEstadoSincronizacion(idEmpresa, Modelos.PrecioEspecial, Estados.Importando);
                string[] lineas = File.ReadAllLines(ruta);

                for (int i = 0; i < lineas.Length; i++)
                {
                    var valores = lineas[i].Split(',');
                    PreciosEspeciales preciosEspeciales = ArmarPrecioEspecial(valores, idEmpresa);

                    dbSet.Add(preciosEspeciales);
                }
                contexto.SaveChanges();
                File.Delete(ruta);
                this.SincronizacionServicio.ActualizarSincronizacion(idEmpresa, Modelos.PrecioEspecial, true);

            }
            catch (NotFoundException ex)
            {

                this.SincronizacionServicio.ActualizaEstadoSincronizacion(idEmpresa, Modelos.PrecioEspecial, Estados.Nada);
                File.Delete(ruta);
                throw ex;
            }
            catch (Exception ex)
            {
                this.SincronizacionServicio.ActualizaEstadoSincronizacion(idEmpresa, Modelos.PrecioEspecial, Estados.Nada);
                File.Delete(ruta);
                throw ex;
            }
        }

        private PreciosEspeciales ArmarPrecioEspecial(string[] valores, int idEmpresa)
        {
            PreciosEspeciales precio = new PreciosEspeciales();
            if (int.TryParse(valores[0], out int codigoCliente))
                precio.CodigoCliente = codigoCliente;

            precio.CodigoProducto = valores[1];

            if (decimal.TryParse(valores[2], out decimal valorPrecio))
                precio.Precio = valorPrecio;

            precio.Descripcion = valores[3];

            return precio;
        }

        public List<PrecioEspecial> Obtener()
        {
            var idEmpresa = AuthServicio.ObtenerEmpresaId();
            var lista = dbSet.Where(p => p.idEmpresa == idEmpresa);

            return lista.Select(p => this.Convertir(p)).ToList();
        }

        public PrecioEspecial ObtenerPorId(int id)
        {
            return this.Convertir(this._ObtenerPorId(id));
        }

        private PreciosEspeciales _ObtenerPorId(int id)
        {
            var idEmpresa = AuthServicio.ObtenerEmpresaId();
            var precio = dbSet.SingleOrDefault(p => p.id == id && p.idEmpresa == idEmpresa);
            if (precio == null)
                throw new NotFoundException(Modelos.PrecioEspecial);

            return precio;
        }

        private PreciosEspeciales Convertir(PrecioEspecial precioEspecial)
        {
            return new PreciosEspeciales()
            {
                Precio = precioEspecial.Precio,
                Descripcion = precioEspecial.Descripcion,
                idEmpresa = AuthServicio.ObtenerEmpresaId(),
                CodigoCliente = precioEspecial.CodigoCliente,
                CodigoProducto = precioEspecial.CodigoProducto,
                id = precioEspecial.Id
            };
        }
        private PrecioEspecial Convertir(PreciosEspeciales preciosEspeciales)
        {
            return new PrecioEspecial
            {
                Id = preciosEspeciales.id,
                IdEmpresa = preciosEspeciales.idEmpresa,
                CodigoCliente = preciosEspeciales.CodigoCliente,
                CodigoProducto = preciosEspeciales.CodigoProducto,
                Descripcion = preciosEspeciales.Descripcion,
                Precio = preciosEspeciales.Precio
            };
        }
        private void Convertir(PreciosEspeciales precio, PrecioEspecial precioEditado)
        {
            precio.Precio = precioEditado.Precio;
            precio.CodigoCliente = precioEditado.CodigoCliente;
            precio.CodigoProducto = precioEditado.CodigoProducto;
            precio.Descripcion = precioEditado.Descripcion;
        }

        public List<PrecioEspecial> Obtener(int codigoCliente, string codigoProducto, int idEmpresa)
        {
            var datos = dbSet.Where(p => p.idEmpresa == idEmpresa && p.CodigoCliente == codigoCliente && p.CodigoProducto == codigoProducto).ToList();

            return datos.Select(p => this.Convertir(p)).ToList();
        }
    }
}
