﻿using Microsoft.AspNet.Identity;
using Servicios.Clases;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Api.Controllers
{
    [RoutePrefix("api/Sincronizacion")]
    [Authorize(Roles = "Administrador,Gestor,Vendedor")]
    public class SincronizacionController : ApiController
    {
        private ISincronizacionServicio SincronizacionServicio;
        public SincronizacionController(ISincronizacionServicio _sincronizacionServicio)
        {
            this.SincronizacionServicio = _sincronizacionServicio;
        }

        // GET: api/Sincronizacion/5
        public IHttpActionResult Get()
        {
            try
            {
                var datos = SincronizacionServicio.Obtener();
                return Ok(datos);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        // POST: api/Sincronizacion
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Sincronizacion/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/Sincronizacion/5
        public void Delete(int id)
        {
        }
    }
}
