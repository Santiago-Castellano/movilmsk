﻿using Entidades;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Web;
using System.IO;
using System.Net.Http.Headers;

namespace Api.Controllers
{
    [RoutePrefix("api/Pedido")]
    [Authorize(Roles = "Administrador,Gestor,Vendedor")]
    public class PedidoController : ApiController
    {
        private IPedidoServicio PedidoServicio;
        private IAuthServicio AuthServicio;
        public PedidoController(IPedidoServicio _PedidoServicio, IAuthServicio _authServicio)
        {
            this.AuthServicio = _authServicio;
            this.PedidoServicio = _PedidoServicio;
        }

        // GET: api/Pedido
        public IHttpActionResult Get()
        {
            try
            {
                var idUser = HttpContext.Current.User.Identity.GetUserId();
                var idEmpresa = this.AuthServicio.ObtenerEmpresaId();
                var pedidos = this.PedidoServicio.Obtener(idUser, idEmpresa);
                return Ok(pedidos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/Pedido/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Pedido
        [HttpPost]
        [Route("Agregar")]
        public IHttpActionResult Post([FromBody]Pedido pedido)
        {
            try
            {
                var pedidoAgregado = this.PedidoServicio.Agregar(pedido);
                return Ok(pedidoAgregado);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        [Route("Editar")]
        // PUT: api/Pedido/5
        public IHttpActionResult Put([FromBody]Pedido pedido)
        {
            try
            {
                var pedidoEditado = this.PedidoServicio.Editar(pedido);
                return Ok(pedidoEditado);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPut]
        [Route("Borrar")]
        // DELETE: api/Pedido/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                this.PedidoServicio.Borrar(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("Exportar")]
        public HttpResponseMessage Exportar()
        {
            try
            {
                var ruta = this.PedidoServicio.Exportar();
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new FileStream(ruta, FileMode.Open);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = Path.GetFileName(ruta);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentLength = stream.Length;
               
                return result;

            }
            catch (Exception ex)
            {
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.BadRequest);
                
                return result;
            }
        }
    }
}
