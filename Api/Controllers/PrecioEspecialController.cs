﻿using Entidades;
using Microsoft.AspNet.Identity;
using Servicios.Clases;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Api.Controllers
{
    [RoutePrefix("api/PrecioEspecial")]
    [Authorize(Roles = "Administrador,Gestor,Vendedor")]
    public class PrecioEspecialController : ApiController
    {
        private IPrecioEspecialServicio PrecioEspecialServicio;
        public PrecioEspecialController(IPrecioEspecialServicio _precioEspecialServicio)
        {
            this.PrecioEspecialServicio = _precioEspecialServicio;
        }
        // GET: api/PrecioEspecial
        /// <summary>
        /// Obtiene todos los Precios Especiales segun la empresa
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get()
        {
            try
            {
                var datos = PrecioEspecialServicio.Obtener();
                return Ok(datos);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Obtener un precio especial por id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/PrecioEspecial/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                var precio = PrecioEspecialServicio.ObtenerPorId(id);
                return Ok(precio);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Agregar un nuevo Precio Especial
        /// </summary>
        /// <param name="precio"></param>
        /// <returns>Precio agregado correctamente</returns>
        // POST: api/PrecioEspecial
        [Route("Agregar")]
        public IHttpActionResult Post([FromBody]PrecioEspecial precio)
        {
            try
            {
                var nuevoPrecio = PrecioEspecialServicio.Agregar(precio);
                return Ok(nuevoPrecio);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Actualizar un Precio
        /// </summary>
        /// <param name="id"></param>
        /// <param name="precio"></param>
        /// <returns>El Precio Editado</returns>
        // PUT: api/PrecioEspecial/5
        [Route("Editar")]
        public IHttpActionResult Put(int id, [FromBody]PrecioEspecial precio)
        {
            try
            {
                var editado = PrecioEspecialServicio.Editar(precio);

                return Ok(editado);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Elimina el Precio
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/PrecioEspecial/5
        [Route("Borrar")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                PrecioEspecialServicio.Borrar(id);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/PrecioEspecial/Importar
        /// <summary>
        /// Importacion de PrecioEspecial
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Importar")]
        public async Task<IHttpActionResult> Importar()
        {
            try
            {
                var c = HttpContext.Current;
                var root = c.Server.MapPath("~/App_Data/" + User.Identity.GetUserId());
                if (!Directory.Exists(root))
                    Directory.CreateDirectory(root);
                var provider = new MultipartFormDataStreamProvider(root);
                await Request.Content.ReadAsMultipartAsync(provider);
                var file = provider.FileData.First();
                var nombre = file.Headers.ContentDisposition.FileName;
                nombre = nombre.Trim('"');
                var localfile = file.LocalFileName;
                var filepath = Path.Combine(root, nombre);
                File.Move(localfile, filepath);
                PrecioEspecialServicio.Importar(filepath);

                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
