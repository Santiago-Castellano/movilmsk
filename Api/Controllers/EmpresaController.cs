﻿using Entidades;
using Servicios.Clases;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace Api.Controllers
{
    [RoutePrefix("api/Empresa")]
    [Authorize(Roles = "Administrador,Gestor,Vendedor")]
    public class EmpresaController : ApiController
    {
        private IEmpresaServicio EmpresaServicio;
        private IAuthServicio AuthServicio;
        public EmpresaController(IEmpresaServicio _empresaServicio,IAuthServicio _authServicio)
        {
            this.AuthServicio = _authServicio;
            this.EmpresaServicio = _empresaServicio;
        }
        /// <summary>
        /// Lista todas las empresas que puede ver el usuario.
        /// </summary>
        /// <returns></returns>
        // GET: api/Empresa
        public IHttpActionResult Get()
        {
            try
            {
                var datos = new List<Empresa>();
                if (User.IsInRole("Administrador"))
                    datos = this.EmpresaServicio.Obtener(string.Empty);
                else
                    datos = this.EmpresaServicio.Obtener(User.Identity.GetUserId());
                return Ok(datos);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Obtener una empresa por su Id
        /// </summary>
        /// <param name="id">Id de la empresa</param>
        /// <returns></returns>
        // GET: api/Empresa/5
        [Authorize(Roles = "Administrador")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var empresa = this.EmpresaServicio.ObtenerPorId(id);
                return Ok(empresa);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Agregar una empresa nueva
        /// </summary>
        /// <param name="empresa">Empresa Agregada</param>
        /// <returns></returns>
        // POST: api/Empresa
        [Route("Agregar")]
        [Authorize(Roles = "Administrador")]
        public IHttpActionResult Post([FromBody]Empresa empresa)
        {
            try
            {
                var nueva = this.EmpresaServicio.Agregar(empresa);
                return Ok(nueva);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Actualizar una empresa
        /// </summary>
        /// <param name="id">Id de la empresa</param>
        /// <param name="empresa">empresa actualizada</param>
        /// <returns></returns>
        [Route("Editar")]
        [HttpPost]
        [Authorize(Roles = "Administrador")]
        public IHttpActionResult Put([FromBody]Empresa empresa)
        {
            try
            {
                var editada = this.EmpresaServicio.Editar(empresa);
                return Ok(editada);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Borrar una empresa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Empresa/5
        [HttpPost]
        [Route("Borrar")]
        [Authorize(Roles = "Administrador")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                this.EmpresaServicio.Borrar(id);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Agrega un usuario existente a una empresa.
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("VincularUsuario")]
        [Authorize(Roles = "Administrador,Gestor")]
        public IHttpActionResult VincularUsuario([FromBody]UsuarioEmpresa usuario)
        {
            try
            {
                if (User.IsInRole("Administrador"))
                    this.EmpresaServicio.VincularUsuario(usuario.Email, usuario.Rol, usuario.IdEmpresa,usuario.NroVendedor);
                else
                {
                    if (this.AuthServicio.ExisteVinculo(User.Identity.GetUserId(), usuario.IdEmpresa))
                        this.EmpresaServicio.VincularUsuario(usuario.Email, usuario.Rol, usuario.IdEmpresa,usuario.NroVendedor);
                    else
                        return BadRequest(Mensaje.NoExiste(Modelos.Empresa));
                }

                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Quita permiso de un usuario a una empresa.
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DesvincularUsuario")]
        [Authorize(Roles = "Administrador,Gestor")]
        public IHttpActionResult DesvincularUsuario([FromBody]UsuarioEmpresa usuario)
        {
            try
            {
                if (User.IsInRole("Administrador"))
                    this.EmpresaServicio.DesvincularUsuario(usuario.Email, usuario.IdEmpresa);
                else
                {
                    if (this.AuthServicio.ExisteVinculo(User.Identity.GetUserId(), usuario.IdEmpresa))
                        this.EmpresaServicio.DesvincularUsuario(usuario.Email, usuario.IdEmpresa);
                    else
                        return BadRequest(Mensaje.NoExiste(Modelos.Empresa));
                }

                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Obtiene los usuarios por empresa.
        /// </summary>
        /// <param name="idEmpresa">Empresa que se quiere obtener los usuarios</param>
        /// <returns></returns>
        [HttpGet]
        [Route("ObtenerUsuariosEmpresa")]
        [Authorize(Roles = "Administrador,Gestor")]
        public IHttpActionResult ObtenerUsuariosEmpresa(int idEmpresa)
        {
            try
            {
                
                var datos = new List<UsuarioEmpresa>();
                if (User.IsInRole("Administrador"))
                    datos = this.EmpresaServicio.ObtenerUsuariosEmpresa(idEmpresa);
                else
                {
                    if (this.AuthServicio.ExisteVinculo(User.Identity.GetUserId(),idEmpresa))
                        datos = this.EmpresaServicio.ObtenerUsuariosEmpresa(idEmpresa);
                    else
                        return BadRequest(Mensaje.NoExiste(Modelos.Empresa));
                }

                return Ok(datos);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

    }
}
