﻿using Entidades;
using Microsoft.AspNet.Identity;
using Servicios.Clases;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Api.Controllers
{
    [RoutePrefix("api/Cliente")]
    [Authorize(Roles = "Administrador,Gestor,Vendedor")]
    public class ClienteController : ApiController
    {
        private IClienteServicio ClienteServicio;
        public ClienteController(IClienteServicio _clienteServicio)
        {
            this.ClienteServicio = _clienteServicio;
        }

        // GET: api/Cliente
        /// <summary>
        /// Obtiene todos los Clientes que pertenecen a la misma empresa del usuario
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get()
        {
            try
            {
                var idUser = HttpContext.Current.User.Identity.GetUserId();
                var datos = ClienteServicio.Obtener(idUser);
                return Ok(datos);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Obtener los datos de un cliente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Cliente/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                var cliente = this.ClienteServicio.ObtenerPorId(id);
                return Ok(cliente);
            }
            catch(NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Agregar un nuevo Cliente
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns>Cliente agregado correctamente</returns>
        // POST: api/Cliente
        [Route("Agregar")]
        public IHttpActionResult Post([FromBody]Cliente cliente)
        {
            try
            {
                var nuevoCliente = this.ClienteServicio.Agregar(cliente);
                return Ok(nuevoCliente);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Actualizar un Cliente
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cliente"></param>
        /// <returns>El Cliente Editado</returns>
        // PUT: api/Cliente/5
        [Route("Editar")]
        public IHttpActionResult Put(int id, [FromBody]Cliente cliente)
        {
            try
            {
                var editado = this.ClienteServicio.Editar(cliente);

                return Ok(editado);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Elimina el cliente
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Cliente/5
        [Route("Borrar")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                this.ClienteServicio.Borrar(id);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/Cliente/Importar
        /// <summary>
        /// Importacion de clientes
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Importar")]
        public async Task<IHttpActionResult> Importar()
        {
            try
            {
                var c = HttpContext.Current;
                var root = c.Server.MapPath("~/App_Data/" + User.Identity.GetUserId());
                if (!Directory.Exists(root))
                    Directory.CreateDirectory(root);
                var provider = new MultipartFormDataStreamProvider(root);
                await Request.Content.ReadAsMultipartAsync(provider);
                var file = provider.FileData.First();
                var nombre = file.Headers.ContentDisposition.FileName;
                nombre = nombre.Trim('"');
                var localfile = file.LocalFileName;
                var filepath = Path.Combine(root, nombre);
                File.Move(localfile, filepath);
                ClienteServicio.Importar(filepath);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
