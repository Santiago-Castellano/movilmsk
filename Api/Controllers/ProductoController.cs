﻿using Entidades;
using Microsoft.AspNet.Identity;
using Servicios.Clases;
using Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Api.Controllers
{
    [RoutePrefix("api/Producto")]
    [Authorize(Roles = "Administrador,Gestor,Vendedor")]
    public class ProductoController : ApiController
    {
        private IProductoServicio ProductoServicio;
        public ProductoController(IProductoServicio _productoServicio)
        {
            this.ProductoServicio = _productoServicio;
        }
        // GET: api/Producto
        /// <summary>
        /// Obtiene todos los Productos que pertenecen a la misma empresa del usuario
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get(int? codigoCliente =null)
        {
            try
            {
                var datos = ProductoServicio.Obtener(codigoCliente);
                return Ok(datos);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/Producto/GetLineas
        /// <summary>
        /// Obtiene todas las lineas que pertenecen a la misma empresa del usuario
        /// </summary>
        /// <returns></returns>
        [Route("getLineas")]
        public IHttpActionResult GetLineas()
        {
            try
            {
                var datos = ProductoServicio.ObtenerLineas();
                return Ok(datos);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Obtener los datos de un Producto
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Producto/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                var producto = ProductoServicio.ObtenerPorId(id);
                return Ok(producto);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Agregar un nuevo Producto
        /// </summary>
        /// <param name="producto"></param>
        /// <returns>Producto agregado correctamente</returns>
        // POST: api/Producto
        [Route("Agregar")]
        public IHttpActionResult Post([FromBody]Producto producto)
        {
            try
            {
                var nuevoProducto = ProductoServicio.Agregar(producto);
                return Ok(nuevoProducto);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Actualizar un Producto
        /// </summary>
        /// <param name="id"></param>
        /// <param name="producto"></param>
        /// <returns>El Producto Editado</returns>
        // PUT: api/Producto/5
        [Route("Editar")]
        public IHttpActionResult Put(int id, [FromBody]Producto producto)
        {
            try
            {
                var editado = ProductoServicio.Editar(producto);

                return Ok(editado);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        /// <summary>
        /// Elimina el Producto
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Producto/5
        [Route("Borrar")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                ProductoServicio.Borrar(id);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/Producto/Importar
        /// <summary>
        /// Importacion de Productos
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Importar")]
        public async Task<IHttpActionResult> Importar()
        {
            try
            {
                var c = HttpContext.Current;
                var root = c.Server.MapPath("~/App_Data/" + User.Identity.GetUserId());
                if (!Directory.Exists(root))
                    Directory.CreateDirectory(root);
                var provider = new MultipartFormDataStreamProvider(root);
                await Request.Content.ReadAsMultipartAsync(provider);
                var file = provider.FileData.First();
                var nombre = file.Headers.ContentDisposition.FileName;
                nombre = nombre.Trim('"');
                var localfile = file.LocalFileName;
                var filepath = Path.Combine(root, nombre);
                File.Move(localfile, filepath);
                ProductoServicio.Importar(filepath);

                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

    }
}
