using Servicios.Clases;
using Servicios.Interfaces;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace Api
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IClienteServicio, ClienteServicio>();
            container.RegisterType<IEmpresaServicio, EmpresaServicio>();
            container.RegisterType<IAuthServicio, AuthServicio>();
            container.RegisterType<ISincronizacionServicio, SincronizacionServicio>();
            container.RegisterType<IProductoServicio, ProductoServicio>();
            container.RegisterType<IPrecioEspecialServicio, PrecioEspecialServicio>();
            container.RegisterType<IPedidoServicio, PedidoServicio>();


            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}